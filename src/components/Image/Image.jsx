import styled from 'styled-components'

export default styled.div`
	width: ${(p) => p.width ? p.width : '100%'};
	height: ${(p) => p.height ? p.height : '100%'};
	${({ bg }) => bg ? `background-image: url(${ bg });` : ``};
	background-size: ${({ size }) => size ? size : 'contain'};
	background-repeat: no-repeat;
	background-position: center;
	padding: 0;
	cursor: pointer;
`;