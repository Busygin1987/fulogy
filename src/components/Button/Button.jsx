import React from 'react';
import styled from 'styled-components';

const Button = styled.div`
    min-height: 30px;
    min-width: 40px;
    background-color: ${(props) => props.bgc ? props.bgc : ''};
    display: flex;
    align-items: center;
    justify-content: start;
`;

const ButtonComponent = ({ children, clickHandler }) => {
    return (
        <Button onClick={clickHandler}>
            {children}
        </Button>
    )
}

export default ButtonComponent;