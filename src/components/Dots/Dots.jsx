import React from 'react';
import styled from 'styled-components';

import { AppContext } from '../../App';

const Dot = styled.span`
    height: 12px;
    width: 12px;
    border-radius: 3px;
    background-color: ${(props) => props.active ? '#fff' : 'rgba(0, 0, 0, 0)'};
    border: 1px solid #fff;
    cursor: pointer;
`;

const WrapContainer = styled.div`
    height: 20px;
    min-width: 80px;
    position: absolute;
    bottom: ${(props) => props.bottom ? props.bottom : '30px'};
    display: flex;
    justify-content: space-between;
`;

const Dots = () => {
    const { state: { images: { current, big: pictures, slideNumber }}, dispatch } = React.useContext(AppContext);

    const handleClick = (index) => {
        dispatch({ type: 'SET_CURRENT_SLIDE', payload: index });
    }

    return (
        <WrapContainer>
            {pictures[current] &&
                pictures[current].map((item, index) => {
                    return <Dot key={item} onClick={() => handleClick(index)} active={index === slideNumber} />
                })
            }
        </WrapContainer>
    )
}

export default Dots;