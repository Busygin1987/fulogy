import styled from 'styled-components';

export default styled.div`
    color: ${(props) => props.color === 'white' ? '#fff' : '#000'};
    font-size: ${(props) => props.fs ? props.fs + 'px' : '20px'};
    margin-bottom: ${(props) => props.mb ? props.mb + 'px' : ''};
`;