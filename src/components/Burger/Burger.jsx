import React from 'react';
import styled, { css } from 'styled-components';

const MenuIcon = styled.div`
    width: 30px;
    height: 4px;
    background-color: rgba(255, 255, 255, .8);
    position: relative;
    cursor: pointer;
    :before {
        position: absolute;
        left: 0;
        top: -10px;
        content: '';
        width: 30px;
        height: 4px;
        display: block;
        background-color: rgba(255, 255, 255, .8);
        transition: transform .2s ease-in, top .2s linear .2s;
    }

    :after {
        position: absolute;
        left: 0;
        top: 10px;
        content: '';
        width: 30px;
        height: 4px;
        display: block;
        background-color: rgba(255, 255, 255, .8);
        transition: transform .2s ease-in, top .2s linear .2s;
    }

    ${({ active }) => active && css`
        background-color: transparent;
        :before {
            transform: rotate(45deg);
            top: 0;
            transition: top .2s linear, transform .2s ease-in .2s;
        }

        :after {
            transform: rotate(-45deg);
            top: 0;
            transition: top .2s linear, transform .2s ease-in .2s;
        }
    `}
`;

const WrapIcon = styled.div`
    width: 30px;
    height: 30px;
    cursor: pointer;
    display: flex;
    align-items: center;
`;

const Burger = ({ handleShowMenu }) => {
    const [active, setActive] = React.useState(false)

    const clickHandler = () => {
        setActive(!active);
        handleShowMenu(!active);
    }

    return (
        <WrapIcon onClick={clickHandler}>
            <MenuIcon active={active} />
        </WrapIcon>
    )
};

export default Burger;