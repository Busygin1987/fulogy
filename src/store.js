export const initialState = {
    options: {
        classProduct: 'Standart',
        power: '59',
        lightPower: '3459 Люмен = 7.5 ламп накаливания по 40Вт',
        warranty: 3,
        mounting: 'Да',
        totalAmount: '2594'
    },
    images: {
        slideNumber: 0,
        current: '',
        big: {
            'Мягкий': [ 'Slide 1 (Мягкий)', 'Slide 2 (Мягкий)', 'Slide 3 (Мягкий)' ],
            'Дневной': [ 'Slide 1 (Дневной)', 'Slide 2 (Дневной)', 'Slide 3 (Дневной)' ],
            'Холодный': [ 'Slide 1 (Холодный)', 'Slide 2 (Холодный)', 'Slide 3 (Холодный)' ]
        },
        small: [ 'Мягкий', 'Дневной', 'Холодный' ]
    },
    descriptionProduct: [
        `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce euismod feugiat luctus. Phasellus dapibus nisl sed libero scelerisque, id iaculis quam volutpat. Phasellus et erat arcu. Nullam vestibulum odio ipsum, nec euismod diam euismod quis. Phasellus lacinia leo a volutpat vehicula. Quisque mattis fringilla laoreet. Quisque lacinia tellus vitae mollis fermentum. Donec eu nisl laoreet, vehicula turpis a, semper velit. Quisque mauris turpis, commodo euismod mi nec, suscipit ultricies sem. Quisque vehicula libero vel vulputate sagittis. In dictum consectetur nisi, ac tempor ante efficitur id. Suspendisse augue purus, volutpat sit amet vehicula in, blandit ac augue. Donec aliquet ut sapien sed mattis. Morbi fringilla nec justo ut pharetra.`,
        `Curabitur iaculis leo et felis lacinia hendrerit. Sed eu porttitor lorem. Nulla facilisi. Morbi ipsum risus, varius a sagittis vel, consequat quis mi. Morbi aliquam nunc velit. Quisque euismod magna luctus, auctor odio quis, accumsan ligula. Sed sollicitudin vehicula feugiat. Integer id faucibus ligula. Praesent semper, ex eu ultrices vulputate, sapien lorem aliquet risus, eu viverra augue dolor in est. Cras lobortis dignissim laoreet. Aenean efficitur fermentum sapien, quis faucibus eros tincidunt pulvinar. Curabitur imperdiet nunc rutrum nisl vestibulum, in maximus sapien dapibus. Donec mauris ex, venenatis nec maximus quis, semper at ligula. Sed ipsum leo, accumsan et ex vel, aliquam pulvinar dui. Integer sit amet fermentum nisl.`,
        `Proin venenatis, neque id auctor tempus, nunc eros pellentesque odio, id mattis dui tortor sed justo. Maecenas efficitur mattis pellentesque. Curabitur sit amet lorem non ligula ornare lobortis sit amet ut risus. Maecenas quis felis interdum, ullamcorper tellus quis, placerat sem. Nam dolor dui, malesuada vel varius eget, consequat id odio. Sed eget orci lobortis, vestibulum risus ultrices, iaculis nulla. Aenean vel magna ut lectus fringilla pulvinar. Integer magna risus, consequat sit amet placerat sed, vulputate sed ante. Quisque viverra tortor eget faucibus vestibulum. Fusce fringilla turpis in tellus feugiat, sit amet aliquam mauris tincidunt. Aliquam at metus sit amet purus vulputate porttitor. Vivamus convallis mollis blandit. Aenean vitae consequat nisi.`
    ]
}

export const reducer = (state, action) => {
    Object.freeze(state);
    switch (action.type) {
        case 'SET_CURRENT_IMAGE':
            return {
                ...state,
                images: {
                    ...state.images,
                    current: action.payload
                }
            };
        case 'SET_CURRENT_SLIDE':
            return {
                ...state,
                images: {
                    ...state.images,
                    slideNumber: action.payload
                }
            };
        default:
            return state;
        }
};