import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`    
    * {
        margin: 0;
        padding: 0; 
    }
    
	body {
		font-family: 'Roboto', sans-serif;
		box-sizing: border-box;
		margin: 0;
		padding: 0;
		width: 100%;
		height: 100%;
		overflow: hidden;
		position: absolute;
		top: 0;
		left: 0;
		outline: 1px solid;
	}

	#root {
		user-select: none;
		height: 100%;
		width: 100%;
	}
`;
