import React from 'react';
import styled from 'styled-components';

import { Text } from '../../components/Typography';
import Button from '../../components/Button';
import Image from '../../components/Image';
import back from '../../images/back.png';

const WrapContainer = styled.div`
    background-color: #fff;
    position: absolute;
    top: 80px;
    left: 0;
    right: 0;
    bottom: 70px;
    /* height: 100%; */
    z-index: 3;
    padding: 20px 100px 20px 30px;
`;

const ButtonBlock = styled.div`
    padding: 10px 0 10px 0;
    cursor: pointer;
    margin-left: -5px;
    opacity: .6;
`;

const Info = ({ data, backHandler = () => {} }) => {
    return (
        <WrapContainer>
            <ButtonBlock>
                <Button clickHandler={backHandler}>
                    <Image bg={back} width={'20px'} height={'20px'} />
                    <Text fs={14}>Вернуться</Text>
                </Button>
            </ButtonBlock>
            {data.map((paragraph) => <Text mb={12} fs={14} key={paragraph.length}>{paragraph}</Text>)}
        </WrapContainer>
    )
}

export default Info;