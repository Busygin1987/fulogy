import React from 'react';
import styled from 'styled-components';
import ContentImage from '../ContentImage';
import ProductOptions from '../ProductOptions';

const WrapContent = styled.div`
    width: 100%;
    height: 75vh;
    display: flex;
`;

const LeftSide = styled.div`
    width: 50%;
    height: 100%;
`;

const RightSide = styled.div`
    width: 50%;
    height: 100%;
    border-left: 1px solid #DCDCDC;
`;

const Content = () => {
    return (
        <WrapContent>
            <LeftSide>
                <ContentImage />
            </LeftSide>
            <RightSide>
                <ProductOptions />
            </RightSide>
        </WrapContent>
    )
}

export default Content;