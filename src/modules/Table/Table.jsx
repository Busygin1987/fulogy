import React from 'react';
import styled from 'styled-components';
import { AppContext } from '../../App';

import { Text } from '../../components/Typography';

const Table = styled.table`
    width: 100%;
    border-collapse: collapse;
    table-layout: fixed;
    border-spacing: 0;
`;

const Body = styled.tbody`
    text-align: start;
`;

export const Row = styled.tr`
    height: 40px;
    border-spacing: 0px;
`;

export const Cell = styled.td`
    text-align: start;
    font-size: 14px;
    margin-top: 8px;
    border-spacing: 0px;
`;

const Border = styled.span`
    background-color: #e0dcdc;
    border: 1px solid #e0dcdc;
    padding: 5px 7px;
    border-radius: 4px;
    -webkit-box-shadow: 0 8px 5px -8px #777;
       -moz-box-shadow: 0 8px 5px -8px #777;
            box-shadow: 0 8px 5px -8px #777;
`;

const TableComponent = () => {
    const {
        state: {
            options: {
                classProduct,
                power,
                lightPower,
                warranty,
                mounting,
                totalAmount
            }
    }} = React.useContext(AppContext);

    return (
        <Table>
            <Body>
                <Row>
                    <Cell><Text fs={14}>Класс:</Text></Cell>
                    <Cell><Border>{classProduct}</Border></Cell>
                </Row>
                <Row>
                    <Cell><Text fs={14}>Потребляемая мощность:</Text></Cell>
                    <Cell><Text fs={14}>{power}Вт</Text></Cell>
                </Row>
                <Row>
                    <Cell><Text fs={14}>Сила света:</Text></Cell>
                    <Cell><Text fs={14}>{lightPower}</Text></Cell>
                </Row>
                <Row>
                    <Cell><Text fs={14}>Гарантия:</Text></Cell>
                    <Cell><Text fs={14}>{warranty} года</Text></Cell>
                </Row>
                <Row>
                    <Cell><Text fs={14}>Монтаж:</Text></Cell>
                    <Cell><Text fs={14}>{mounting}</Text></Cell>
                </Row>
                <Row>
                    <Cell><Text fs={14}>Итого сумма:</Text></Cell>
                    <Cell><Text fs={14}>{totalAmount}</Text></Cell>
                </Row>
            </Body>
        </Table>
    )
}

export default TableComponent;