import React from 'react';
import styled, { css } from 'styled-components';

import { Text } from '../../components/Typography';
import { AppContext } from '../../App';
import Image from '../../components/Image';
import icon from '../../images/current.png';

const FakeImage = styled.div`
    width: 120px;
    height: 60px;
    background-color: ${(props) => props.color ? props.color : ''};
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    position: relative;
    ${({ active }) => active && css`
        box-shadow: 0 4px 10px rgba(0,0,0,0.1), 0 4px 8px rgba(0,0,0,0.15);
        -webkit-transform: scale(1.1);
        -ms-transform: scale(1.1);
        transform: scale(1.1);

    `}
`;

const WrapContainer = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-evenly;
    margin-top: 40px;
`;

const AbsoluteImage = styled(Image)`
    position: absolute;
    top: 5px;
    right: 5px;
`;

const ChoiceImage = ({ data: pictures }) => {
    const { dispatch, state: { images: { current }}} = React.useContext(AppContext);
    return (
        <WrapContainer>
            {pictures.map((picture, index) => {
                return <FakeImage
                        color={'#FAEBD7'}
                        key={index}
                        onClick={() => dispatch({ type: 'SET_CURRENT_IMAGE', payload: picture })}
                        active={current === picture}
                    >
                        <Text fs={14}>{picture}</Text>
                        {current === picture && <AbsoluteImage bg={icon} width={'15px'} height={'15px'} />}
                    </FakeImage>
            })}
        </WrapContainer>
    )
}

export default ChoiceImage;