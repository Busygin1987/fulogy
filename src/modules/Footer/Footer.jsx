import React from 'react';
import styled from 'styled-components';

import { Text } from '../../components/Typography';

const WrapFooter = styled.div`
    height: 70px;
    background-color: #F5F5F5;
    border-top: 1px solid #DCDCDC;
    display: flex;
    padding: 0 40px;
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
`;

const Button = styled.div`
    cursor: pointer;
    width: 12.5%;
    justify-content: center;
    align-items: center;
    display: flex;
    border-left: 1px solid #DCDCDC;
    position: relative;
    background-color: ${(props) => props.active && '#00BFFF'};

    &:last-child {
        border-right: 1px solid #DCDCDC;
    }

    &:before {
        content: '';
        position: absolute;
        bottom: -5px;
        width: 100%;
        height: 1px;
        border-bottom: 10px solid #00BFFF;
    }

    &:hover {
        background-color: #87CEEB;
    }
`;

const Footer = () => {
    const buttonsName = ['Вариант кухни', 'Размеры', 'Сенсор', 'Питающий кабель', 'Блок питания', 'Цвет свечения', 'Монтаж', 'Корзина'];
    const handleClick = (elem) => {
        const buttonsNode = document.querySelectorAll('.footer-button');
        buttonsNode.forEach((node) => {
            node.style.backgroundColor = '';
        })
        elem.currentTarget.style.backgroundColor = '#00BFFF';
    }
    return (
        <WrapFooter>
            {buttonsName.map((name, index) => <Button onClick={handleClick} key={name + index} name={name} className={'footer-button'}>
                <Text fs={14}>{name}</Text>
            </Button>)}
        </WrapFooter>
    )
}

export default Footer;