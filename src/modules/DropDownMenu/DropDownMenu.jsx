import React from 'react';
import styled from 'styled-components';

const WrapMenu = styled.div`
    position: absolute;
    right: 0;
    bottom: 0;
    top: 80px;
    width: 40%;
    display: ${(props) => props.visible ? 'flex' : 'none'};
    z-index: 1;
    flex-direction: column;
    min-height: calc(100vh - 80px);
    align-items: stretch;
`;

const DarkBlock = styled.div`
    background-color: rgba(0, 0, 0, .9);
    width: 100%;
    height: 67px;
    font-size: 24px;
    color: #fff;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const Line = styled.div`
    border-top: 1.5px solid #696969;
`;

const Link = styled.a`
    text-decoration: none;
`;

const DropDownMenu = ({ visible }) => {
    const sections = ['Обучающее видео', 'Оформление заказа', 'Оплата', 'Доставка', 'Гарантия', 'Возврат', 'Контакты', 'Партнерам'];

    return (
        <WrapMenu visible={visible}>
            {sections.map((section) => {
                return (
                    // eslint-disable-next-line jsx-a11y/anchor-is-valid
                    <React.Fragment key={section}>
                        <Link href={'#'}><DarkBlock key={section}>{section}</DarkBlock></Link>
                        <Line />
                    </React.Fragment>
                )
            })}
        </WrapMenu>
    )
}

export default DropDownMenu;