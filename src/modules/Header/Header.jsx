import React from 'react';
import styled from 'styled-components';

import Burger from '../../components/Burger';
import Image from '../../components/Image';
import DropDownMenu from '../../modules/DropDownMenu';
import logo from '../../images/logo_white.svg';
import cart from '../../images/cart2.png';


const WrapHeader = styled.div`
    height: 80px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    background-color: #000;
    padding: 0 30px;
    position: relative;
`;

const ImageBlock = styled.div`
    width: ${(props) => props.w ? props.w + 'px' : '200px'};
    height: ${(props) => props.h ? props.h + 'px' : '40px'};
    margin-top: ${(props) => props.mt ? props.mt + 'px' : ''};
`;

const WrapBurger = styled.div`
    display: flex;
    width: 100px;
    height: 40px;
    justify-content: space-between;
    position: relative;
`;

const Header = () => {
    const [visible, setVisible] = React.useState(false);
    return (
        <WrapHeader>
            <ImageBlock>
                <Image bg={logo} />
            </ImageBlock>
            <WrapBurger>
                <ImageBlock w={48} h={48} mt={-12}>
                    <Image bg={cart} />
                </ImageBlock>
                <Burger handleShowMenu={setVisible} />
            </WrapBurger>
            <DropDownMenu visible={visible} />
        </WrapHeader>
    )
};

export default Header;