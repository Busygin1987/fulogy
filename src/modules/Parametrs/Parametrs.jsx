import React from 'react';
import styled from 'styled-components';

import ChoiceImage from '../ChoiceImage';
import { Text } from '../../components/Typography';
import Info from '../Info';
import { AppContext } from '../../App';

import info from '../../images/info.png';

const SuggestColor = styled.div`
    height: 50px;
    background-color: #00BFFF;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
`;

const WrapContainer = styled.div`
    /* position: relative; */
    height: 100%;
`;

const InfoImage = styled.div`
    height: 55px;
    width: 55px;
    cursor: pointer;
    background: ${(props) => props.bg ? `url(${props.bg})` : ''};
    background-size: contain;
	background-repeat: no-repeat;
	background-position: center;
    /* z-index: 100; */
    position: absolute;
    left: 0;
`;

const Parametrs = () => {
    const [ flagShowInfo, setFlag ] = React.useState(false);
    const { state: { descriptionProduct, images: { small }}} = React.useContext(AppContext);

    const showProductInfo = () => {
        setFlag(!flagShowInfo);
    }

    return (
        <WrapContainer>
            <SuggestColor>
                <InfoImage bg={info} onClick={showProductInfo} />
                <Text color={'white'}>Выберите цвет свечения</Text>
            </SuggestColor>
            { small.length > 0 && <ChoiceImage data={small} />}
            { flagShowInfo && <Info data={descriptionProduct} backHandler={showProductInfo} /> }
        </WrapContainer>
    )
}

export default Parametrs;