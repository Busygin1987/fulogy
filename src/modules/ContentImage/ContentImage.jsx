import React from 'react';
import { AppContext } from '../../App';
import styled from 'styled-components';

import { Text } from '../../components/Typography';
import Dots from '../../components/Dots';

const FakeImage = styled.div`
    width: 600px;
    height: 400px;
    background-color: ${(props) => props.color ? props.color : ''};
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    background-color: #8A8585;
    position: relative;
`;

const WrapContainer = styled.div`
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
`;

const ContentImage = () => {
    const { state: { images: { current, big: pictures, slideNumber }}} = React.useContext(AppContext);

    return (
        <WrapContainer>
            {pictures[current] &&
                <FakeImage>
                    <Text fs={14}>{pictures[current][slideNumber]}</Text>
                    <Dots />
                </FakeImage>
            }
        </WrapContainer>
    )
}

export default ContentImage;