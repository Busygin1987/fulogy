import React from 'react';
import styled from 'styled-components';

import OptionsTable from '../Table';
import Parametrs from '../Parametrs';

const WrapOptions = styled.div`
    width: 100%;
    height: 100%;
`;

const UpperBlock = styled.div`
    height: 60%;
    padding: 15px 40px 15px 25px;
    box-sizing: border-box;
`;

const LowerBlock = styled.div`
    height: 40%;
    box-sizing: border-box;
    /* position: relative; */
`;

const ProductOptions = () => {
    return (
        <WrapOptions>
            <UpperBlock>
                <OptionsTable />
            </UpperBlock>
            <LowerBlock>
                <Parametrs />
            </LowerBlock>
        </WrapOptions>
    )
}

export default ProductOptions;