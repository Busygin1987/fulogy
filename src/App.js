import React from 'react';
import styled from 'styled-components';

import Header from './modules/Header';
import Content from './modules/Content';
import Footer from './modules/Footer';

import { reducer, initialState } from './store';

const WrapPage = styled.div`
  width: 100%;
  min-height: 100vh;
  background-color: #F5F5F5;
`;

export const AppContext = React.createContext();

function App() {
  const [ state, dispatch ] = React.useReducer(reducer, initialState);

  return (
    <AppContext.Provider value={{ state, dispatch }}>
      <WrapPage>
        <Header />
        <Content />
        <Footer />
      </WrapPage>
    </AppContext.Provider>
  );
}

export default App;
